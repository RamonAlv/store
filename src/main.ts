import Vue from 'vue';
import App from './App.vue';
import Login from './views/Login.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import EvaIcons from 'vue-eva-icons'

Vue.use(EvaIcons)
Vue.use(BootstrapVue)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
