import Vue from 'vue';
import Router from 'vue-router';
import Admin from './views/Admin.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
    },
    {
      path: '/cashier',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('./views/register.vue'),
    },
    {
      path: '/',
      name: 'login',
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('./views/Products.vue'),
    },
    {
      path: '/product/add',
      name: 'productAdd',
      component: () => import('./views/Addproduct.vue'),
    },
    {
      path: '/app/:id',
      name: 'app',
      component: () => import('./App.vue'),
    },
    {
      path: '/inventario',
      name: 'inventario',
      component: () => import('./views/Inventarios.vue'),
    },
    {
      path: '/transaccion',
      name: 'transaccion',
      component: () => import('./views/Transaccion.vue'),
    },
  ],
});
